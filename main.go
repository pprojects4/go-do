package main

import (
	"example/rustydvorak/go-do/models"
	"example/rustydvorak/go-do/routes"
	"net/http"
	"log"
)

func main() {
	models.GetTodos()

	http.HandleFunc("/todos/create/", routes.AddTodo)
	http.HandleFunc("/", routes.IndexView)
	log.Fatal(http.ListenAndServe(":8000", nil))
}
