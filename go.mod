module example/rustydvorak/go-do

go 1.21.9

require (
	github.com/jmoiron/sqlx v1.4.0 // indirect
	github.com/lib/pq v1.10.9 // indirect
)
