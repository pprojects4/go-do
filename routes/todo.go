package routes

import (
	"example/rustydvorak/go-do/models"
	"fmt"
	"html/template"
	"net/http"
)

type Index struct {
	Name string
}

func IndexView(w http.ResponseWriter, req *http.Request) {
	fmt.Println("------ Home ------")
	todos := models.GetTodos()
	tmp := template.Must(template.ParseFiles("templates/index.html"))
	tmp.Execute(w, todos)
}

func AddTodo(w http.ResponseWriter, req *http.Request) {
	fmt.Println(">>>>>>> Add <<<<<<<")
	tmpl := template.Must(template.ParseFiles("templates/todo/create.html"))
	tmpl.Execute(w, nil)
}
