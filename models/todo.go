package models

import (
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Todo struct {
	Id          int        `db:"id"`
	Name        string     `db:"name"`
	Description string     `db:"description"`
	Completed   bool       `db:"completed"`
	Deadline    *time.Time `db:"deadline"`
	Created     *time.Time `db:"created"`
	Modified    *time.Time `db:"modified"`
}

func GetTodos() []Todo {
	db, err := sqlx.Connect("postgres", "user=postgres dbname='todo-db' sslmode=disable")
	if err != nil {
		log.Fatal("Unable to connect to database", err)
	}
	todos := []Todo{}
	dbErr := db.Select(&todos, "select * from Todo where completed=$1;", false)
	if dbErr != nil {
		log.Fatal("Error reading from db:", dbErr)
	}
	return todos
}
