-- Add migration script here
-- create posts table
CREATE TABLE Todo(
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  description TEXT,
  completed BOOL DEFAULT false,
  deadline timestamptz NULL,
  created i NOT NULL DEFAULT timezone('utc', now()),
  modified timestamptz NOT NULL DEFAULT timezone('utc', now())
);